// console.log('hi')

/*
QUIZ
1.  You can create an array using square brackets ([]) and separating the values with commas. You can also create an empty array by simply using empty square brackets

2. To access the first element of an array, you can use bracket notation and the index 0.

3. To access the last element of an array, you can use the index length-1 with bracket notation.

4. indexOf()

5. forEach()

6. map()

7. every()

8. some()

9. False

10. True

*/

// ACTIVITY

// 1. 
		function addToEnd(arr, str) {
		  if (typeof str !== 'string') {
		    return "error - can only add strings to an array";
		  }
		  arr.push(str);
		  return arr;
		}

		let students = ['John', 'Jane', 'Mike'];

		let updatedStudents = addToEnd(students, 'Ryan');
		console.log(updatedStudents); 

		let errorMessage = addToEnd(students, 123);
		console.log(errorMessage); 


// 2. 
		function addToStart(arr, str) {
		  if (typeof str !== 'string') {
		    return "error - can only add strings to an array";
		  }
		  arr.unshift(str);
		  return arr;
		}

		let studentsTwo = ['John', 'Jane', 'Mike'];

		let updatedStudentsTwo = addToStart(studentsTwo, 'Tess');
		console.log(updatedStudentsTwo);

		let errorMessageTwo = addToStart(studentsTwo, 123);
		console.log(errorMessageTwo);

// 3. 
		function elementChecker(arr, str) {
		  if (arr.length === 0) {
		    return "error - passed in array is empty";
		  }
		  for (let i = 0; i < arr.length; i++) {
		    if (arr[i] === str) {
		      return true;
		    }
		  }
		  return false;
		}

		let studentsThree = ['John', 'Jane', 'Mike'];

		let result1 = elementChecker(studentsThree, 'Jane');
		console.log(result1);

		let result2 = elementChecker(studentsThree, 'Tess');
		console.log(result2);

		let errorMessageThree = elementChecker([], 'Tess');
		console.log(errorMessageThree);

// 4. 
		function checkAllStringsEnding(arr, char) {
		  if (arr.length === 0) {
		    return "error - array must NOT be empty";
		  }
		  if (!arr.every(elem => typeof elem === 'string')) {
		    return "error - all array elements must be strings";
		  }
		  if (typeof char !== 'string') {
		    return "error - 2nd argument must be of data type string";
		  }
		  if (char.length !== 1) {
		    return "error - 2nd argument must be a single character";
		  }
		  return arr.every(elem => elem.charAt(elem.length - 1) === char);
		}

		let studentsFour = ['Jane', 'Mike', 'Steve', 'Kate'];

		let result11 = checkAllStringsEnding(studentsFour, 'e');
		console.log(result11);

		let result21 = checkAllStringsEnding(studentsFour, 'i');
		console.log(result21);

		let result31 = checkAllStringsEnding([], 'e');
		console.log(result31);

		let result41 = checkAllStringsEnding(['Jane', 'Mike', 123], 'e');
		console.log(result41);

		let result51 = checkAllStringsEnding(studentsFour, 'ee');
		console.log(result51);

		let result61 = checkAllStringsEnding(studentsFour, 5);
		console.log(result61);

// 5. 

		function stringLengthSorter(arr) {
		  // Check if all elements in the array are strings
		  if (!arr.every((elem) => typeof elem === "string")) {
		    return "error - all array elements must be strings";
		  }
		  
		  // Sort the array by the length of the strings
		  arr.sort((a, b) => a.length - b.length);
		  
		  return arr;
		}


		const studentsSeven = ["Alice", "Bob", "Charlie", "David"];

		const sortedStudentsSeven = stringLengthSorter(studentsSeven);

		console.log(sortedStudentsSeven);
		// Output: ["Bob", "Alice", "David", "Charlie"]



		const studentsEight = ["Alice", "Bob", "Charlie", "David", 123];

		const sortedStudentsEight = stringLengthSorter(studentsEight);

		console.log(sortedStudentsEight);
		// Output: "error - all array elements must be strings"


// 6.

function startsWithCounter(arr, char) {
  // Check if the array is empty
  if (arr.length === 0) {
    return "error - array must NOT be empty";
  }
  
  // Check if all elements in the array are strings
  if (!arr.every((elem) => typeof elem === "string")) {
    return "error - all array elements must be strings";
  }
  
  // Check if the second argument is a string
  if (typeof char !== "string") {
    return "error - 2nd argument must be of data type string";
  }
  
  // Check if the second argument is a single character
  if (char.length !== 1) {
    return "error - 2nd argument must be a single character";
  }
  
  // Count the number of elements in the array that start with the character argument
  const count = arr.reduce((acc, curr) => {
    if (curr.startsWith(char)) {
      return acc + 1;
    } else {
      return acc;
    }
  }, 0);
  
  return count;
}
const studentsNine = ["Alice", "Bob", "Charlie", "David", "John", "Jade", "jason"];

const count = startsWithCounter(studentsNine, "J");

console.log(count);
// Output: 2





// 7. 

		function likeFinder(arr, str) {
		  // Check if array is empty
		  if (arr.length === 0) {
		    return "error - array must NOT be empty";
		  }
		  
		  // Check if all elements are strings
		  for (let i = 0; i < arr.length; i++) {
		    if (typeof arr[i] !== "string") {
		      return "error - all array elements must be strings";
		    }
		  }
		  
		  // Check if search string is a string
		  if (typeof str !== "string") {
		    return "error - 2nd argument must be of data type string";
		  }
		  
		  // Create new array with matching elements
		  let result9 = [];
		  for (let i = 0; i < arr.length; i++) {
		    if (arr[i].toLowerCase().includes(str.toLowerCase())) {
		      result9.push(arr[i]);
		    }
		  }
		  
		  return result9;
		}

		// Test with students array and search string "jo"
		let studentsFive = ["John", "Mary", "Joseph", "Joan", "Joe"];
		console.log(likeFinder(studentsFive, "jo")); // Output: ["Joseph", "Joan", "Joe"]

// 8.

		function randomPicker(arr) {
		  if (arr.length === 0) {
		    return "error - array must NOT be empty";
		  }
		  const randomIndex = Math.floor(Math.random() * arr.length);
		  return arr[randomIndex];
		}

		const studentsSix = ["John", "Jane", "Bob", "Alice"];

		console.log(randomPicker(studentsSix));
